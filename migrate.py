# -*- coding: utf-8 -*-
import sys
from flask_script import Manager, Command
from flask_migrate import Migrate, MigrateCommand

from engagements.application import create_app, get_config
from engagements.extensions import db

from engagements.models import security, actors, locations


def parse_options():
    """Parses command line options for Flask.

    Returns:
    Config instance to pass into create_app().
    """
    # Figure out which class will be imported.
    if '--config' in sys.argv:
        loc = sys.argv.index('--config')
        val = sys.argv[loc+1]
        sys.argv = sys.argv[0:loc] + sys.argv[loc+1:len(sys.argv)-1]
        config_class_string = 'engagements.config.' + val
    else:
        config_class_string = 'engagements.config.Config'
    config_obj = get_config(config_class_string)

    return config_obj


class InitCommand(Command):
    """
    Initializes the database entries for basic functionality.
    """
    def run(self):
        role = app.user_datastore.find_or_create_role("Admin", description="Engagements Administrator")
        if security.User.query.count() == 0:
            user = app.user_datastore.create_user(
                email=app.config.get('ADMINS', ['dev@null.test'])[0],
                password='admin',
                timezone="CET",
                locale="sl_si"
            )
            app.user_datastore.add_role_to_user(user, role)
        db.session.commit()

        if actors.Faction.query.count() == 0:
            a = [
                actors.Faction(
                    id=1,
                    name="State"
                ),
                actors.Faction(
                    id=2,
                    name="Freedom"
                )
            ]
            db.session.add_all(a)
        db.session.commit()

        if locations.Region.query.count() == 0:
            r = [
                locations.Region(
                    id=1,
                    name="Europe"
                ),
                locations.Region(
                    id=2,
                    name="North America"
                ),
                locations.Region(
                    id=3,
                    name="South America"
                ),
                locations.Region(
                    id=4,
                    name="Asia"
                ),
                locations.Region(
                    id=5,
                    name="India"
                ),
                locations.Region(
                    id=6,
                    name="Middle East"
                ),
                locations.Region(
                    id=7,
                    name="Africa"
                )
            ]
            db.session.add_all(r)
        db.session.commit()

        if actors.Organization.query.count() == 0:
            o = [
                actors.Organization(
                    id=1,
                    name="Kommando Spezialkräfte",
                    region_id=1,
                    faction_id=1
                ),
                actors.Organization(
                    id=2,
                    name="Special Air Service",
                    region_id=1,
                    faction_id=1
                ),
                actors.Organization(
                    id=3,
                    name="Spetsnaz",
                    region_id=1,
                    faction_id=1
                ),
                actors.Organization(
                    id=4,
                    name="Delta Force",
                    region_id=2,
                    faction_id=1
                ),
                actors.Organization(
                    id=5,
                    name="Fuerzas Especiales",
                    region_id=3,
                    faction_id=1
                ),
                actors.Organization(
                    id=6,
                    name="Sayeret Matkal",
                    region_id=6,
                    faction_id=1
                ),
                actors.Organization(
                    id=7,
                    name="Sa'ka Forces",
                    region_id=7,
                    faction_id=1
                ),
                actors.Organization(
                    id=8,
                    name="Para Commandos",
                    region_id=5,
                    faction_id=1
                ),
                actors.Organization(
                    id=9,
                    name="Zhōngguó tèzhǒng bùduì",
                    region_id=4,
                    faction_id=1
                ),
                actors.Organization(
                    id=10,
                    name="Global Freedom Fighters",
                    region_id=4,
                    faction_id=2
                )
            ]
            db.session.add_all(o)
        db.session.commit()

        if actors.Position.query.count() == 0:
            p = [
                actors.Position(
                    id=1,
                    name="Commander"
                ),
                actors.Position(
                    id=2,
                    name="Intelligence"
                ),
                actors.Position(
                    id=3,
                    name="Medical"
                ),
                actors.Position(
                    id=4,
                    name="Engineering"
                ),
                actors.Position(
                    id=5,
                    name="Assault"
                ),
                actors.Position(
                    id=6,
                    name="Sniper"
                )
            ]
            db.session.add_all(p)
        db.session.commit()

app = create_app(parse_options())

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)
manager.add_command('init', InitCommand)

if __name__ == '__main__':
    manager.run()
