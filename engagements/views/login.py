# -*- coding: utf-8 -*-
import base64

from flask import request, jsonify, current_app
from werkzeug.exceptions import Unauthorized

from engagements.core.route import crossdomain


@current_app.route('/auth/login', methods=['POST'])
@crossdomain(origin=None)
def login():
    data = request.get_json()
    email = data.get('email')
    password = data.get('password')
    if not email or not password:
        raise Unauthorized('Wrong username and/or password.')
    else:
        user = current_app.user_datastore.get_user(email)
        if user and user.check_password(password):
            token = user.get_auth_token()
            token = base64.b64encode(token.decode('ascii') + ':')
            return jsonify({'access_token': token})
    raise Unauthorized('Wrong username and/or password.')
