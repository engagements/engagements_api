# -*- coding: utf-8 -*-
from engagements.application import create_app, get_config

app = create_app(get_config('engagements.config.Config'))

if __name__ == "__main__":
    app.run()
