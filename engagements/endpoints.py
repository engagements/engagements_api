# -*- coding: utf-8 -*-
import importlib
import inspect
import pkgutil

from eve_sqlalchemy.decorators import registerSchema

from engagements.config import HardCoded
from engagements.extensions import db

app_models = None
if not app_models:
    # Location of Eve-SQLAlchemy models
    import engagements.models as app_models

all_endpoints = {}


def register_schema(model, name=None):
    if not name:
        if hasattr(model, "Meta") and hasattr(model.Meta, "resource_name"):
            name = model.Meta.resource_name
        else:
            name = model.__name__.lower()

    registerSchema(name)(model)

    model._eve_schema[name]['id_field'] = HardCoded.ID_FIELD
    model._eve_schema[name]['item_lookup_field'] = HardCoded.ITEM_LOOKUP_FIELD

    if hasattr(model, "Meta"):
        meta = dict([(attr, val,) for attr, val in model.Meta.__dict__.iteritems() if not attr.startswith("_")])
        model._eve_schema[name].update(meta)

    all_endpoints[name] = model._eve_schema[name]

if app_models:
    for importer, modname, ispkg in pkgutil.iter_modules(app_models.__path__):
        m = importlib.import_module(app_models.__name__ + "." + modname)
        for name, obj in inspect.getmembers(m):
            if inspect.isclass(obj) and db.Model in obj.__mro__:
                register_schema(obj)

# Space for leftover custom imports
# register_schema(package.module.Model)
