from eve import Eve


class Engagements(Eve):
    def add_url_rule(self, rule, endpoint=None, view_func=None, **options):
        if rule and rule.startswith('/'):
            super(Engagements, self).add_url_rule(rule, endpoint=endpoint, view_func=view_func, **options)
