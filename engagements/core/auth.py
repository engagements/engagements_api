# -*- coding: utf-8 -*-
from flask import abort, current_app, request

from eve import auth

from engagements.models import security


class EnTokenAuth(auth.TokenAuth):
    def check_auth(self, token, allowed_roles, resource, method):
        email = security.User.verify_auth_token(token)
        if email:
            user = current_app.user_datastore.get_user(email)
            return user.is_authorized(allowed_roles)
        else:
            return False

    def authenticate(self):
        abort(401, description='Please provide proper credentials')

    def authorized(self, allowed_roles, resource, method):
        auth_header = request.authorization
        return auth_header and self.check_auth(auth_header.username, allowed_roles, resource, method)
