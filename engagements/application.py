# -*- coding: utf-8 -*-
from importlib import import_module
import os
from yaml import load

from flask import g, request
from flask_babel import Babel
from flask_security import Security, SQLAlchemyUserDatastore

from eve_sqlalchemy.validation import ValidatorSQL

import engagements as app_root
from engagements.core import app as eapp, auth
from engagements.endpoints import all_endpoints
from engagements.views import all_views
from engagements.extensions import celery, db, SQL, mail, redis
from engagements.models import security as security_models

APP_ROOT_FOLDER = os.path.abspath(os.path.dirname(app_root.__file__))
STATIC_FOLDER = os.path.join(APP_ROOT_FOLDER, 'static')
REDIS_SCRIPTS_FOLDER = os.path.join(APP_ROOT_FOLDER, 'redis_scripts')


def get_config(config_class_string, yaml_files=None):
    """Load the Flask config from a class.

    Positional arguments:
    config_class_string -- string representation of a configuration class that will be loaded (e.g.
        'vulcan_web.config.Production').
    yaml_files -- List of YAML files to load. This is for testing, leave None in dev/production.

    Returns:
    A class object to be fed into app.config.from_object().
    """
    config_module, config_class = config_class_string.rsplit('.', 1)
    config_class_object = getattr(import_module(config_module), config_class)
    config_obj = config_class_object()

    # Expand some options.
    celery_fmt = 'engagements.tasks.{}'
    db_fmt = 'engagements.models.{}'
    if getattr(config_obj, 'CELERY_IMPORTS', False):
        config_obj.CELERY_IMPORTS = [celery_fmt.format(m) for m in config_obj.CELERY_IMPORTS]
    for definition in getattr(config_obj, 'CELERYBEAT_SCHEDULE', dict()).values():
        definition.update(task=celery_fmt.format(definition['task']))
    for script_name, script_file in getattr(config_obj, 'REDIS_SCRIPTS', dict()).items():
        config_obj.REDIS_SCRIPTS[script_name] = os.path.join(REDIS_SCRIPTS_FOLDER, script_file)

    # Load additional configuration settings.
    yaml_files = yaml_files or [f for f in [
        os.path.join('/', 'etc', 'engagements', 'config.yml'),
        os.path.abspath(os.path.join(APP_ROOT_FOLDER, '..', 'config.yml')),
        os.path.join(APP_ROOT_FOLDER, 'config.yml'),
    ] if os.path.exists(f)]
    additional_dict = dict()
    for y in yaml_files:
        with open(y) as f:
            additional_dict.update(load(f.read()))

    # Merge the rest into the Flask app config.
    for key, value in additional_dict.items():
        setattr(config_obj, key, value)

    return config_obj


def create_app(config_obj):
    config_dict = dict([(k, getattr(config_obj, k)) for k in dir(config_obj) if not k.startswith('_')])
    config_dict['DOMAIN'] = all_endpoints

    app = eapp.Engagements(
        auth=auth.EnTokenAuth,
        settings=config_dict,
        validator=ValidatorSQL,
        data=SQL,
        static_folder=STATIC_FOLDER
    )

    app.user_datastore = SQLAlchemyUserDatastore(db, security_models.User, security_models.Role)
    Security(app, app.user_datastore)

    db.init_app(app)
    redis.init_app(app)
    celery.init_app(app)
    mail.init_app(app)

    with app.app_context():
        for name, module in all_views.iteritems():
            import_module(module)

    with app.app_context():
        import_module('engagements.middleware')

    app.babel = Babel(app)

    @app.babel.localeselector
    def get_locale():
        user = getattr(g, 'user', None)
        if user is not None:
            return user.locale

        return request.accept_languages.best_match(app.config['BABEL_LANGUAGES'])

    @app.babel.timezoneselector
    def get_timezone():
        user = getattr(g, 'user', None)
        if user is not None:
            return user.timezone
        else:
            return 'CET'

    return app
