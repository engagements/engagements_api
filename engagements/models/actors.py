# -*- coding: utf-8 -*-
# App Imports
from engagements.models.helpers import DefaultMixin
from engagements.extensions import db

from engagements.models import locations

operator_team = db.Table('operator_team', db.Model.metadata,
                      db.Column('operator_id', db.Integer, db.ForeignKey('operators.id')),
                      db.Column('team_id', db.Integer, db.ForeignKey('teams.id')))


class Operator(db.Model, DefaultMixin):
    __tablename__ = "operators"

    name = db.Column(db.String(80))
    loyalty = db.Column(db.Integer)
    health = db.Column(db.Integer)

    points = db.Column(db.Integer)

    data = db.Column(db.TEXT)

    employer_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                      onupdate='CASCADE', ondelete='CASCADE'),
                            )
    employer = db.relationship("User", active_history=True, innerjoin=True, lazy='joined',
                               backref=db.backref("employees",
                                                  cascade="all, delete-orphan",
                                                  passive_deletes=True)
                               )


class Team(db.Model, DefaultMixin):
    __tablename__ = "teams"

    name = db.Column(db.String(80))

    owner_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                   onupdate='CASCADE', ondelete='CASCADE'),
                         )
    owner = db.relationship("User", active_history=True, innerjoin=True, lazy='joined',
                            backref=db.backref("teams",
                                               cascade="all, delete-orphan",
                                               passive_deletes=True)
                            )


class Organization(db.Model, DefaultMixin):
    __tablename__ = "organizations"

    name = db.Column(db.String(80))

    faction_id = db.Column(db.Integer, db.ForeignKey('factions.id'))
    faction = db.relationship("Faction", active_history=True, innerjoin=True, lazy='joined',
                              backref=db.backref("organizations")
                              )

    region_id = db.Column(db.Integer, db.ForeignKey('regions.id'))
    region = db.relationship(locations.Region, active_history=True, innerjoin=True, lazy='joined',
                             backref=db.backref("organizations")
                             )


class Relation(db.Model, DefaultMixin):
    __tablename__ = "relations"

    data = db.Column(db.TEXT)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                  onupdate='CASCADE', ondelete='CASCADE'),
                        )
    user = db.relationship("User", active_history=True, innerjoin=True, lazy='joined',
                           backref=db.backref("relations",
                                              cascade="all, delete-orphan",
                                              passive_deletes=True)
                           )

    organization_id = db.Column(db.Integer, db.ForeignKey('organizations.id',
                                                          onupdate='CASCADE', ondelete='CASCADE'),
                                )
    organization = db.relationship("Organization", active_history=True, innerjoin=True, lazy='joined',
                                   backref=db.backref("relations",
                                                      cascade="all, delete-orphan",
                                                      passive_deletes=True)
                                   )


class Faction(db.Model, DefaultMixin):
    __tablename__ = "factions"

    name = db.Column(db.String(80))


class Position(db.Model, DefaultMixin):
    __tablename__ = "positions"

    name = db.Column(db.String(80))


class TeamPosition(db.Model, DefaultMixin):
    __tablename__ = "team_positions"

    team_id = db.Column(db.Integer, db.ForeignKey('teams.id',
                                                  onupdate='CASCADE', ondelete='CASCADE'),
                        )
    team = db.relationship("Team", active_history=True, innerjoin=True, lazy='joined',
                           backref=db.backref("team_positions",
                                              cascade="all, delete-orphan",
                                              passive_deletes=True)
                           )

    operator_id = db.Column(db.Integer, db.ForeignKey('operators.id',
                                                      onupdate='CASCADE', ondelete='CASCADE'),
                            )
    operator = db.relationship("Operator", active_history=True, innerjoin=True, lazy='joined',
                               backref=db.backref("team_positions",
                                                  cascade="all, delete-orphan",
                                                  passive_deletes=True)
                               )

    position_id = db.Column(db.Integer, db.ForeignKey('positions.id'))
    position = db.relationship("Position", active_history=True, innerjoin=True, lazy='joined',
                               backref=db.backref("team_positions")
                               )
