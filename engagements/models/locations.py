# -*- coding: utf-8 -*-
# App Imports
from engagements.models.helpers import DefaultMixin
from engagements.extensions import db


class Region(db.Model, DefaultMixin):
    __tablename__ = "regions"

    name = db.Column(db.String(80))


class Base(db.Model, DefaultMixin):
    __tablename__ = "bases"

    name = db.Column(db.String(80))

    data = db.Column(db.TEXT)

    owner_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                   onupdate='CASCADE', ondelete='CASCADE'),
                         )
    owner = db.relationship("User", active_history=True, innerjoin=True, lazy='joined',
                            backref=db.backref("bases",
                                               cascade="all, delete-orphan",
                                               passive_deletes=True)
                            )

    region_id = db.Column(db.Integer, db.ForeignKey('regions.id'))
    region = db.relationship("Region", active_history=True, innerjoin=True, lazy='joined',
                             backref=db.backref("bases")
                             )
