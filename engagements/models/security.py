# -*- coding: utf-8 -*-
import itsdangerous

# Core Imports
from flask import current_app
from flask_security import UserMixin, RoleMixin
from flask_security.utils import encrypt_password, verify_password

from sqlalchemy.orm import validates

# App Imports
from engagements.models.helpers import DefaultMixin
from engagements.extensions import db

user_roles = db.Table('user_roles', db.Model.metadata,
                      db.Column('user_id', db.Integer, db.ForeignKey('users.id')),
                      db.Column('role_id', db.Integer, db.ForeignKey('roles.id')))


class Role(db.Model, DefaultMixin, RoleMixin):
    __tablename__ = "roles"

    class Meta:
        resource_methods = []

    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, DefaultMixin, UserMixin):
    __tablename__ = "users"

    class Meta:
        allowed_filters = ['email']

    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))
    active = db.Column(db.Boolean)
    confirmed_at = db.Column(db.DateTime)

    timezone = db.Column(db.String(45), nullable=False, default='CET')
    locale = db.Column(db.String(45), nullable=False, default='sl_si')

    roles = db.relationship('Role', secondary=user_roles,
                            backref=db.backref('users', lazy='dynamic'))

    def get_auth_token(self, expiration=24*60*60):
        s = itsdangerous.TimedJSONWebSignatureSerializer(current_app.config['SECRET_KEY'], expires_in=expiration)
        ret = s.dumps({'email': self.email})
        return ret

    def is_authorized(self, role_names):
        if not role_names:
            return True
        for role in role_names:
            if self.has_role(role):
                return True
        return False

    @staticmethod
    def verify_auth_token(token, expiration=24*60*60):
        s = itsdangerous.TimedJSONWebSignatureSerializer(current_app.config['SECRET_KEY'], expires_in=expiration)
        try:
            data = s.loads(token)
        except itsdangerous.SignatureExpired:
            return None
        except itsdangerous.BadSignature:
            return None
        return data['email']

    @staticmethod
    def encrypt(password):
        return encrypt_password(password)

    @validates('password')
    def _set_password(self, column, value):
        return self.encrypt(value)

    def check_password(self, password):
        if not self.password:
            return False
        return verify_password(password, self.password)
