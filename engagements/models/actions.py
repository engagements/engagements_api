# -*- coding: utf-8 -*-
# App Imports
from engagements.models.helpers import DefaultMixin
from engagements.extensions import db


class Mission(db.Model, DefaultMixin):
    __tablename__ = "missions"

    name = db.Column(db.String(80))

    data = db.Column(db.TEXT)

    user_id = db.Column(db.Integer, db.ForeignKey('users.id',
                                                  onupdate='CASCADE', ondelete='CASCADE'),
                        )
    user = db.relationship("User", active_history=True, innerjoin=True, lazy='joined',
                           backref=db.backref("missions",
                                              cascade="all, delete-orphan",
                                              passive_deletes=True)
                           )
