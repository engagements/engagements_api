# -*- coding: utf-8 -*-
"""Redis keys used throughout the entire application (Flask, etc.)."""

# Email throttling.
EMAIL_THROTTLE = 'engagements:email_throttle:{md5}'  # Lock.
