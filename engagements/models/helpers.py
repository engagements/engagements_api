# -*- coding: utf-8 -*-
"""Convenience functions which interact with SQLAlchemy models."""

from sqlalchemy import func
from sqlalchemy import Column, Integer, DateTime, String
from sqlalchemy.sql import operators

from engagements.extensions import db

QUERY_SEP = "__"

_oper = {
    'eq': operators.eq,
    'gt': operators.gt,
    'lt': operators.lt,
    'ge': operators.ge,
    'le': operators.le,
    'ne': operators.ne,
    'contains': operators.contains_op,
}


class DefaultMixin(object):
    __table_args__ = dict(mysql_charset='utf8', mysql_engine='InnoDB')

    id = Column(Integer, primary_key=True, autoincrement=True, nullable=True)

    _created = Column(DateTime, default=func.now())
    _updated = Column(DateTime, default=func.now(), onupdate=func.now())
    _etag = Column(String(40))


def count(column, value, glob=False):
    """Counts number of rows with value in a column. This function is case-insensitive.

    Args:
        column: -- the SQLAlchemy column object to search in (e.g. Table.a_column).
        value: -- the value to search for, any string.
        glob: -- enable %globbing% search (default False).

    Returns:
    Number of rows that match. Equivalent of SELECT count(*) FROM.
    """
    query = db.session.query(func.count('*'))
    if glob:
        query = query.filter(column.ilike(value))
    else:
        query = query.filter(func.lower(column) == value.lower())
    return query.one()[0]


def filter_query(query_obj, **filters):
    for name, value in filters.iteritems():
        col = None
        op = _oper['eq']
        tokens = name.split(QUERY_SEP)
        for tok in tokens:
            if tok in _oper:
                if col is None:
                    raise ValueError("No column supplied")
                op = _oper[tok]
            else:
                col = getattr(query_obj._primary_entity.mapper.mapped_table.columns, tok)

        query_obj = query_obj.filter(op(col, value))

    return query_obj
