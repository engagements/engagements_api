# -*- coding: utf-8 -*-
import os
from urllib import quote_plus


class HardCoded(object):
    """Constants used throughout the application.

    All hard coded settings/data that are not actual/official configuration options for Flask, Celery, or their
    extensions goes here.
    """
    ADMINS = ['primoz.jeras@offblast.si']
    ENVIRONMENT = property(lambda self: self.__class__.__name__)
    MAIL_EXCEPTION_THROTTLE = 24 * 60 * 60

    ID_FIELD = 'id'
    ITEM_LOOKUP_FIELD = 'id'
    RESOURCE_METHODS = ['GET', 'POST']
    ITEM_METHODS = ['GET', 'PATCH', 'DELETE', 'PUT']

    ALLOWED_FILTERS = []
    VALIDATE_FILTERS = True
    VALIDATION_ERROR_AS_STRING = True
    UPSERT_ON_PUT = False

    URL_PREFIX = 'api'

    _SQLALCHEMY_DATABASE_DATABASE = 'engagements_development'
    _SQLALCHEMY_DATABASE_HOSTNAME = '192.168.1.1'
    _SQLALCHEMY_DATABASE_USERNAME = 'engagements'
    _SQLALCHEMY_DATABASE_PASSWORD = 'engagements_api'

    SECURITY_LOGIN_URL = None
    SECURITY_LOGOUT_URL = None


class CeleryConfig(HardCoded):
    """Configurations used by Celery only."""
    CELERYD_PREFETCH_MULTIPLIER = 1
    CELERYD_TASK_SOFT_TIME_LIMIT = 20 * 60  # Raise exception if task takes too long.
    CELERYD_TASK_TIME_LIMIT = 30 * 60  # Kill worker if task takes way too long.
    CELERY_ACCEPT_CONTENT = ['json']
    CELERY_ACKS_LATE = True
    CELERY_DISABLE_RATE_LIMITS = True
    CELERY_IMPORTS = ('pypi',)
    CELERY_RESULT_SERIALIZER = 'json'
    CELERY_TASK_RESULT_EXPIRES = 10 * 60  # Dispose of Celery Beat results after 10 minutes.
    CELERY_TASK_SERIALIZER = 'json'
    CELERY_TRACK_STARTED = True
    CELERY_BROKER_URL='redis://localhost:6379',
    CELERY_RESULT_BACKEND='redis://localhost:6379'


class Config(CeleryConfig):
    """Default Flask configuration inherited by all environments. Use this for development environments."""
    DEBUG = True
    TESTING = False
    PROFILE = False
    SECRET_KEY = "this_is_so_much_secret"

    MAIL_SERVER = 'smtp.localhost.test'
    MAIL_DEFAULT_SENDER = 'admin@demo.test'
    MAIL_SUPPRESS_SEND = True

    REDIS_URL = 'redis://127.0.0.1/0'

    X_DOMAINS = ['http://127.0.0.1:5000', 'http://127.0.0.1:9000']
    X_HEADERS = ['Content-Type', "Authorization"]
    X_ALLOW_CREDENTIALS = True

    SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
    SECURITY_PASSWORD_SALT = 'engagements'

    SQLALCHEMY_DATABASE_URI = property(lambda self: 'mysql://{u}:{p}@{h}/{d}'.format(
        d=quote_plus(self._SQLALCHEMY_DATABASE_DATABASE), h=quote_plus(self._SQLALCHEMY_DATABASE_HOSTNAME),
        p=quote_plus(self._SQLALCHEMY_DATABASE_PASSWORD), u=quote_plus(self._SQLALCHEMY_DATABASE_USERNAME)
    ))

    BABEL_LANGUAGES = ('en_uk', 'sl_si',)

    APP_ROOT = os.path.dirname(os.path.abspath(__file__))
    APP_STATIC = os.path.join(APP_ROOT, 'static')


class Testing(Config):
    DEBUG = False
    TESTING = True
    CELERY_ALWAYS_EAGER = True
    REDIS_URL = 'redis://localhost/1'

    _SQLALCHEMY_DATABASE_DATABASE = 'engagements_testing'


class Heroku(Config):
    DEBUG = False
    MAIL_SUPPRESS_SEND = False
    STATICS_MINIFY = True

    SECRET_KEY = os.environ.get('SECRET_KEY', Config.SECRET_KEY)

    SQLALCHEMY_POOL_RECYCLE = 20
    SQLALCHEMY_DATABASE_URI = property(lambda self:
                                       os.environ.get('CLEARDB_DATABASE_URL',
                                                      Config.SQLALCHEMY_DATABASE_URI.fget(self)))
