# -*- coding: utf-8 -*-
"""Flask middleware definitions. This is also where template filters are defined.

To be imported by the application.current_app() factory.
"""

import locale
from logging import getLogger
import os

from celery.signals import task_failure, worker_process_init
from flask import current_app, render_template, request
from markupsafe import Markup

from engagements.core.email import send_exception
from engagements.extensions import db

LOG = getLogger(__name__)


# Fix Flask-SQLAlchemy and Celery incompatibilities.
@worker_process_init.connect
def celery_worker_init_db(**_):
    """Initialize SQLAlchemy right after the Celery worker process forks.

    This ensures each Celery worker has its own dedicated connection to the MySQL database. Otherwise
    one worker may close the connection while another worker is using it, raising exceptions.

    Without this, the existing session to the MySQL server is cloned to all Celery workers, so they
    all share a single session. A SQLAlchemy session is not thread/concurrency-safe, causing weird
    exceptions to be raised by workers.

    Based on http://stackoverflow.com/a/14146403/1198943
    """
    LOG.debug('Initializing SQLAlchemy for PID {}.'.format(os.getpid()))
    db.init_app(current_app)


# Send email when a Celery task raises an unhandled exception.
@task_failure.connect
def celery_error_handler(sender, exception, **_):
    exception_name = exception.__class__.__name__
    task_module = sender.name
    send_exception('{} exception in {}'.format(exception_name, task_module))
